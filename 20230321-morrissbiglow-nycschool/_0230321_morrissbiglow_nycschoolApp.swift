//
//  _0230321_morrissbiglow_nycschoolApp.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import SwiftUI

@main
struct _0230321_morrissbiglow_nycschoolApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
