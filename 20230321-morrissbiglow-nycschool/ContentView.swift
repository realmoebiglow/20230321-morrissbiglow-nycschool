//
//  ContentView.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        SchoolListView(viewModel: SchoolListViewModel())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
