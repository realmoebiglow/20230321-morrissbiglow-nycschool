//
//  Network Manager.swift
//  20230321-morrissbiglow-nycschool
//
//Created by Morriss Biglow 3 / 21 / 2023
//

import Foundation

class NetworkService {
    private let decoder = JSONDecoder()
    
    func fetch<T: Decodable>(_ endpoint: URL, responseType: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        var request = URLRequest(url: endpoint)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(URLError(.badServerResponse)))
                return
            }
            
            guard (200...299).contains(httpResponse.statusCode) else {
                completion(.failure(URLError(.badServerResponse)))
                return
            }
            
            guard let data = data else {
                completion(.failure(URLError(.badServerResponse)))
                return
            }
            
            do {
                let decodedResponse = try self.decoder.decode(responseType, from: data)
                completion(.success(decodedResponse))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
}


struct NetworkEndpoint<Response: Decodable> {
    let url: URL
    let responseType: Response.Type
    init(url: URL, responseType: Response.Type) {
        self.url = url
        self.responseType = responseType
    }
}

struct NetworkEndpoints {
    
    static func score(dbn: String) -> NetworkEndpoint<[SATScore]> {
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)")!
        return NetworkEndpoint(url: url, responseType: [SATScore].self)
    }
    
    static func schools() -> NetworkEndpoint<[School]> {
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        return NetworkEndpoint(url: url, responseType: [School].self)
    }
}



