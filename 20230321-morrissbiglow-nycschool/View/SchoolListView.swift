//
//  SchoolListView.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import SwiftUI

struct SchoolListView: View {
    @ObservedObject var viewModel: SchoolListViewModel
    
    var body: some View {
        NavigationView {
            List(viewModel.schools, id: \.dbn) { school in
                NavigationLink(destination: SchoolDetailView(viewModel: SchoolDetailViewModel(school: school))) {
                    Text(school.schoolName ?? "")
                }
            }
            .navigationBarTitle("Schools")
        }
        .onAppear {
            viewModel.loadSchools()
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView(viewModel: SchoolListViewModel())
    }
}
