//
//  SchoolDetailViewModel.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import Foundation

class SchoolDetailViewModel: ObservableObject {
    @Published var school: School
    @Published var satScores: SATScore?
    private let networkService = NetworkService()

    init(school: School) {
        self.school = school
        fetchSATScores()
    }

    private func fetchSATScores() {
        let endpoint = NetworkEndpoints.score(dbn: school.dbn).url
        networkService.fetch(endpoint, responseType: [SATScore].self) { [weak self] result in
            switch result {
            case .success(let scores):
                self?.satScores = scores.first
            case .failure(let error):
                print("Error fetching SAT scores: \(error.localizedDescription)")
            }
        }
    }
}

