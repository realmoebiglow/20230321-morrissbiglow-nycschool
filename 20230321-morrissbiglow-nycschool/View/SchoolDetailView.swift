//
//  SchoolDetailView.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import SwiftUI

struct SchoolDetailView: View {
    @ObservedObject var viewModel: SchoolDetailViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            VStack(alignment: .leading, spacing: 10) {
                Text("SAT Scores")
                    .font(.title)
                if let mathScore = viewModel.satScores?.satMathAvgScore {
                    Text("Math: \(mathScore)")
                }
                if let readingScore = viewModel.satScores?.satCriticalReadingAvgScore {
                    Text("Reading: \(readingScore)")
                }
                if let writingScore = viewModel.satScores?.satWritingAvgScore {
                    Text("Writing: \(writingScore)")
                }
            }
            Divider()
            VStack(alignment: .leading, spacing: 10) {
                Text("Other Info")
                    .font(.title)
                if let schoolName = viewModel.school.schoolName {
                    Text("School Name: \(schoolName)")
                }
                if let address = viewModel.school.primaryAddressLine1,
                   let city = viewModel.school.city,
                   let state = viewModel.school.stateCode,
                   let zip = viewModel.school.zip {
                    Text("Address: \(address), \(city), \(state) \(zip)")
                }
                if let website = viewModel.school.website {
                    Text("Website: \(website)")
                }
            }
            Spacer()
        }
        .padding()
        .navigationBarTitle(viewModel.school.schoolName ?? "")
    }
}
