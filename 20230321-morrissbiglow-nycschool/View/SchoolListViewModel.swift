//
//  SchoolListViewModel.swift
//  20230321-morrissbiglow-nycschool
//
//  Created by Consultant on 3/21/23.
//

import Foundation

class SchoolListViewModel: ObservableObject {
    @Published var schools: [School] = []
    private let networkService = NetworkService()
    
    init() {
        
    }
    
    func loadSchools() {
        let endpoint = NetworkEndpoints.schools().url
        networkService.fetch(endpoint, responseType: [School].self) { result in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.schools = response
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}


