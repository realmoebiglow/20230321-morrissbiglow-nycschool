//
//  School.swift
//  20230321-morrissbiglow-nycschool
//Created by Morriss Biglow 3 / 21 / 2023



import Foundation

struct School: Decodable {
    let dbn: String
    let schoolName: String?
    let buildingCode: String?
    
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    
    let website: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case buildingCode = "building_code"
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
        case website
    }
}

extension School: Identifiable, Hashable {
    var id: String { dbn }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

